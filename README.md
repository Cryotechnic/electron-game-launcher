<p align="center"><img src="https://xanderfrangos.com/media/previews/den.jpg" width="680" /></p>

# Disclaimer
This project was not developed by me. I am modifying it for my own personal use for a project that I am working on during my free time. I am not selling this nor accepting 
donations for this. Please contact XanderFrangos on Github if you wish to use this commercially.

# Purpose
I am intending to rewrite the game selection as a micro-site that has all patch notes/guides on it. The overall look will stay the same. The name will change to
better suit my development needs.

# electron-game-launcher
Not functional at the moment. **An in-browser demo is available [here](https://xanderfrangos.com/electron-game-launcher/).** Use a controller (Xbox/PS4) or keyboard (arrow keys, Enter, Backspace) to navigate. Chrome or Firefox required.

